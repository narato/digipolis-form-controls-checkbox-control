(function (_, ng) {
    'use strict';
    ng
    .module('formRenderer.controls')
    .service('frFieldTypeService-form-renderer-checkbox-control', [

        /**
         * frFieldTypeCheckboxService
         * @return {Object} - field type configuration object
         */
        function () {
            return {
                link: function (scope, element, attrs, ctrl) {

                    var optionsWatcher;
                    var frFormCtrl = ctrl[0];
                    var dataStore;

                    /**
                     * @function
                     * @name  initialize -----------------------------
                     */
                    function initialize() {
                        if (!checkModel()) {
                            return;
                        }
                        if (scope.field.to.dataStore) {
                            dataStore = frFormCtrl.getDataStore(scope.field.to.dataStore);
                            if (dataStore.length > 0) {
                                scope.field.to.valueOptions = dataStore;
                            }
                        }
                        scope.field.to.multiple = checkMultiple();
                        if (scope.field.to.multiple && !scope.field.to.valueOptions) {
                            optionsWatcher = scope.$watch('field.to.valueOptions', function (newValue, oldValue) {
                                if (newValue) {
                                    initialize();
                                    optionsWatcher();

                                }
                            });
                            return;
                        }

                        prepareValueOptions(scope.ngModelInit);
                    }
                    /**
                     * @function
                     * @name  checkModel ----------------------------
                     * @description check if the model is not undefined
                     * @return {Boolean}
                     */
                    var checkModel = function checkModel() {
                        if (scope.ngModel === undefined) {
                            console.error('The fr-checkbox directive requires a model');
                            return false;
                        }
                        return true;
                    };

                    /**
                     * @function
                     * @name  checkMultiple ---------------------------
                     * @description check if it is multiple checkbox or not
                     * @return {Boolean}
                     */
                    var checkMultiple = function checkMultiple() {
                        if (scope.field.to.valueOptions && scope.field.to.valueOptions.length > 0) {
                            return true;
                        } else {
                            return false;
                        }
                    };

                    /**
                     * @function
                     * @name  parseValueOptions -----------------------
                     * @description - en
                     * @param  {Array} modelInit - initial model value
                     */
                    function prepareValueOptions(modelInit) {
                        if (_.isArray(modelInit)) {
                            _.forEach(modelInit, function (modelOption) {
                                var option = _.find(scope.field.to.valueOptions, function (option) {
                                    return option.key === modelOption;
                                });
                                option.checked = option ? true : false;
                            });
                        }
                    }

                    /**
                     * @function
                     * @name  resetValueOptions ----------------------
                     * @description - reset value options with the initial
                     * model value
                     * @param  {Array} modelInit - initial model value
                     * @return {Boolean}
                     */
                    function resetValueOptions(modelInit) {
                        _.forEach(scope.field.to.valueOptions, function (option) {
                            option.checked = false;
                        });
                        prepareValueOptions(modelInit);
                        return true;
                    }

                    /**
                     * @function
                     * @name  toggleEditMode --------------------------
                     * @description - toggles the state property `editMode`
                     */
                    scope.toggleEditMode = function toggleEditMode() {
                        scope.field.state.editMode = !scope.field.state.editMode;
                        // reset the model with the initial value
                        scope.ngModel = _.cloneDeep(scope.ngModelInit);
                        resetValueOptions(scope.ngModelInit);
                    };

                    /**
                     * @function
                     * @name  getValueFromOptions -----------------------
                     * @description - get a value from the value options by key
                     * @param  {String} key - value option key
                     * @return {String} - option value
                     */
                    scope.getValueFromOptions = function getValueFromOptions(key) {
                        var option = _.find(scope.field.to.valueOptions, function (option) {
                            return option.key === key;
                        });
                        return option && option.value;
                    };

                    /**
                     * @function
                     * @name booleanToReadableString -----------------
                     * @description convert true -> JA and false -> Nee
                     * @param  {Boolean} bool - Boolean
                     * @return {String} - 'JA' or 'Nee'
                     */
                    scope.booleanToReadableString = function booleanToReadableString(bool) {
                        return bool ? 'Ja' : 'Nee';
                    };

                    /**
                     * @function
                     * @name  updateModel ----------------------------
                     * @description update the model
                     * @param  {Object} option 'key value pair'
                     */
                    scope.updateModel = function updateModel(option) {
                        if (typeof scope.ngModel === 'string') {
                            scope.ngModel = [];
                        }
                        if (option.checked === undefined) {
                            console.warn('can\'t update the checkbox list model, options checked = ' + option.checkbox);
                        }
                        var inModel = scope.ngModel.indexOf(option.key);
                        if (option.checked) {
                            if (inModel < 0) {
                                scope.ngModel.push(option.key);
                            }
                        } else if (!option.checked) {
                            if (inModel > -1) {
                                scope.ngModel.splice(inModel, 1);
                            }
                        }
                        return scope.ngModel;
                    };
                    initialize();
                }
            };
        }
    ]);
})(window._, window.angular);
